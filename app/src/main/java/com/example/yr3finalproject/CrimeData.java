package com.example.yr3finalproject;

import java.io.File;
import java.util.Scanner;

public class CrimeData extends MapActivity {
    private static Scanner x;

    public CrimeData() {
        String filepath = "R.raw.thames_valley_street.csv";
        String searchTerm = "Reading";
        readRecord(filepath, searchTerm);
    }
    public static void readRecord(String searchTerm,String filepath) {
        boolean found = false;
        String ID = ""; String name1 = ""; String age = "";
        try {
            x = new Scanner(new File(filepath));
            x.useDelimiter("[,\n]");

            while(x.hasNext() && !found) {
                ID = x.next();
                name1 = x.next();
                age = x.next();

                if (ID.equals(searchTerm)) {
                    found = true;
                }
            }

            if (found) {
                System.out.println("ID " + ID + " name " + name1 + " age " + age);
            }
            else {
                System.out.println("Record not found");
            }
        }
        catch (Exception e) {
                System.out.println("error");
        }
    }
}

